//
// Created by Jan Niklas Hollenbeck on 12.12.18.
//

#ifndef LAB_8_ATM_USERINTERFACE_H
#define LAB_8_ATM_USERINTERFACE_H

#include "language.h"
#include <vector>
#include <string>
#include <iostream>
#include "json.hpp"

class UserInterface {
public:
    ///Sets up the User Interface
    UserInterface(std::string lang){
        std::ifstream i("/Users/Kyodu/Documents/Master_Informatik/SPLE/SWPLE/Lab_8_ATM/languages/" + lang + ".json");
        nlohmann::json j;
        i >> j;
        for (nlohmann::json::iterator it = j["languages"].begin(); it != j["languages"].end(); ++it) {
            std::string file = *it;
            language *tmp = new language(file);
            languages.push_back(tmp);

        }
        activelang = languages.at(0);
        activecount = 0;
        runUI();

    };

private:
    void runUI();
    void langswitch();
    std::vector<language*> languages;
    language *activelang;
    int activecount;
    std::string supportedlanguages;

};


#endif //LAB_8_ATM_USERINTERFACE_H
