//
// Created by Jan Niklas Hollenbeck on 12.12.18.
//

#ifndef LAB_8_ATM_LANGUAGE_H
#define LAB_8_ATM_LANGUAGE_H


#include <string>
#include <fstream>
#include <iostream>
#include "json.hpp"

class language {
public:
    language(std::string file) {
        std::ifstream i("/Users/Kyodu/Documents/Master_Informatik/SPLE/SWPLE/Lab_8_ATM/languages/" + file + "text.json");
        nlohmann::json j;
        i >> j;
        this->greeting = j["greeting"];
        this->warningInput = j["warning"];
        this->supportedlanguages = j["supportedlanguages"];
    }

    std::string greeting;
    std::string warningInput;
    std::string supportedlanguages;

};


#endif //LAB_8_ATM_LANGUAGE_H
