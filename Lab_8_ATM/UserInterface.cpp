//
// Created by Jan Niklas Hollenbeck on 12.12.18.
//

#include "UserInterface.h"

void UserInterface::runUI() {

    std::cout << "##############################" <<std::endl;
    std::cout << this->activelang->supportedlanguages <<std::endl;
    while(true){
        std::cout << "##############################" <<std::endl;
        std::cout << this->activelang->greeting <<std::endl;
        std::string input;
        std::cin >> input;
        if(input == "u"){
            this->langswitch();
        }else if(input == "v") {
            exit(0);
        }else
        {
            std::cout << this->activelang->warningInput << "'" << input << "'"<< std::endl;
            std::cout << "##############################" <<std::endl;
        }
    }
}
void UserInterface::langswitch() {
    if (languages.size()-1>activecount){
        activecount = activecount + 1;
    }else{
        activecount = 0;
    }
    activelang = languages.at(activecount);
}