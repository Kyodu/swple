cmake_minimum_required(VERSION 3.9)
project(SWPLE)

set(CMAKE_CXX_STANDARD 11)

add_executable(SWPLE main.cpp)