# Manual Generator
This is a Software to automaticly generate Manuals.
#### How to generate a new Manual?
  - Copy the "configs/BaseConfig.cpp" and Rename it according to the Name of the Radio
  - edit the new config file with the questions provided in it and chose the features
  - In the RadioManual.cpp add A new #ifdef and include the new config file 
  - To generate the Manual run the createManual.sh with two parameters the definded Keyword and the output file  e.g: "./createManual.sh DARMSTADT darmstadt.html"
#### Orthogonal variability model
![Model for the Core asset](https://gitlab.com/Kyodu/swple/blob/master/SWPLE_orthogonal_variability_model.png)