#!/bin/sh
#clear

# param1 is target, param2 is output-file

NoOfParams="0"

if [ $2 ]; then
    NoOfParams="2"
fi

case $NoOfParams in

    2) gcc -E -P -Wall -D $1 -o $2 _RadioManual.cpp
      ;;

   *)
      echo "No Radio specified. Try again!"
      echo "1st parameter specifies which Configuration should be used (e.g DARMSTADT)"
      echo "2nd parameter specifies filename for output (e.g. Darmstadt.html)"
      ;;
esac
