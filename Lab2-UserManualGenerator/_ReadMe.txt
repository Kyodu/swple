In this folder you find 
- a windows batch file (createManual.bat) that creates the manuals on windows
- a linux batch file (createManual.sh) that creates the manuals on linux
- 3 text files with the manual pages for 3 car radios (Darmstadt.txt, Dieburg.txt, Griesheim.txt)
- a cpp file (_RadioManual.cpp) that uses the C-preprocessor to include the txt-files depending on parameters passed

This requires the GNU C-preprocessor. So please make sure that the command gcc is working in a console window.
gcc comes for example with the Code::Blocks IDE (if you choose the installation including the compiler).
Maybe you have to adopt your path-Variable to make sure that gcc is found.

Be aware that the scripts compare strings. So it is important to use the correct spelling and to take care of uppercase letters.

It your environment is properly set up you can start the script (.sh or .bat) in a console window as follows
createManual.bat DARMSTADT darmstadt.html on windows or
createManual.sh DARMSTADT darmstadt.html on linux

If you need some help about C-Preprocessor commands, look at
https://de.wikibooks.org/wiki/C-Programmierung:_Pr%C3%A4prozessor
