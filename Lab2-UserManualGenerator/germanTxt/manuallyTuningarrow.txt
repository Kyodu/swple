<h4>Starten Sie die manuelle Suche</h4>

    <p>durch Drücken von <span class="key">&larr;</span> oder <span class="key">&rarr;</span>,
	die Frequenz bewegt sich in Schritten nach oben oder unten.</p>

	<p> Wenn Sie <span class="key">&larr;</span> oder <span class="key">&rarr;</span> gedrückt halten,
		dann wird die Frequenz schnell durchlaufen.</p>
