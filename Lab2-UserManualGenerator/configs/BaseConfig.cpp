//Name of the Radio:
#define _MODEL name
//In which language should the Manual be?
#define _ENGLISH // _GERMAN
//Does the Radio have a Display?
#define _DISPLAY
//Does the Radio have a Keeypad
#define _NUM_KEYPAD
//Does The Manual Tuning uses the single Arrow? (->/<-)
#define _MAN_TUNING_ARROW
//Or does it use the Double Arrow? (<</>>)
#define _MAN_TUNING_DOUBLE_ARROW
//Does the Radio Support speeding up the manual tuning?
#define _SPEED_UP_TUNING
//Does the Radio Support automatic tuning?
#define _AUTO_TUNING
