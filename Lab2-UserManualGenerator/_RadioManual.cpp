//Depending on what Keeword is used in the bash file the correct config file whil be used
//To add a new Config add a #ifdef Keyword and include the corrosponing Configfile
#ifdef DARMSTADT
  #include "./configs/darmstadt.cpp"
#endif

#ifdef DIEBURG
  #include "./configs/diburg.cpp"
#endif

#ifdef GRIESHEIM
  #include "./configs/griesheim.cpp"
#endif



// Configuration for features to add a new one create it both in english and german
// chose also a new Keeword e.g. _DISPLAY or _GERMAM
#include "./englishTxt/header.txt"

#ifdef _ENGLISH

#ifdef _DISPLAY
    #include "./englishTxt/display.txt"
#endif

#ifdef _SPEED_UP_TUNING
    #include "./englishTxt/speedUpTuning.txt"
#endif

#ifdef _AUTO_TUNING
    #include "./englishTxt/automaticTuning.txt"
#endif

#ifdef _MAN_TUNING_ARROW
    #include "./englishTxt/manuallyTuningarrow.txt"
#endif

#ifdef _MAN_TUNING_DOUBLE_ARROW
    #include "./englishTxt/manuallyTuningdoublearrow.txt"
#endif

#ifdef _NUM_KEYPAD
    #include "./englishTxt/numericKeypad.txt"
#endif

#endif

//German textiles
//Options for the manual
#ifdef _GERMAN
#ifdef _DISPLAY
    #include "./germanTxt/display.txt"
#endif

#ifdef _SPEED_UP_TUNING
    #include "./germanTxt/speedUpTuning.txt"
#endif

#ifdef _AUTO_TUNING
    #include "./germanTxt/automaticTuning.txt"
#endif

#ifdef _MAN_TUNING_ARROW
    #include "./germanTxt/manuallyTuningarrow.txt"
#endif

#ifdef _MAN_TUNING_DOUBLE_ARROW
    #include "./germanTxt/manuallyTuningdoublearrow.txt"
#endif

#ifdef _NUM_KEYPAD
    #include "./germanTxt/numericKeypad.txt"
#endif
#endif
#include "./englishTxt/footer.txt"
