/**
 * @(#) PassengerRouteStrategy.h
 */

#ifndef PASSENGERROUTESTRATEGY_H_H
#define PASSENGERROUTESTRATEGY_H_H

#include "RouteStrategy.h"
class PassengerRouteStrategy: public RouteStrategy
{
	
public:
	virtual void calculate( );
	
};

#endif
