/**
 * @(#) DriverInfoSystem.h
 */

#ifndef DRIVERINFOSYSTEM_H_H
#define DRIVERINFOSYSTEM_H_H

#include "RouteStrategy.h"
#include "ObjectRequestBroker.h"
#include "RouteStrategyRegData.h"
#include "Component.h"
class DriverInfoSystem
{

public:
	DriverInfoSystem( );

private:
	RouteStrategy* myRouteStrategy;

	RouteStrategyRegData * myRouteStrategyRegData;
};

#endif
