/**
 * @(#) ObjectRequestBroker.h
 */

#ifndef OBJECTREQUESTBROKER_H_H
#define OBJECTREQUESTBROKER_H_H

class Component;
class ObjectRequestBroker
{

public:
	static Component* getComponent( long ID );
	static void subscribe( long ID, Component* Compo );

private:
	static Component * myCompos;

};

#endif
