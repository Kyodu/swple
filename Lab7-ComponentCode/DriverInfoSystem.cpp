/**
 * @(#) DriverInfoSystem.cpp
 */


#include "DriverInfoSystem.h"
#include "RouteStrategyRegData.h"

DriverInfoSystem::DriverInfoSystem( )
{
    //get the ID for the Route Strategy component

    // get the reference to the route strategy from the ORB
    // do not forget the cast to a route strategy
    ObjectRequestBroker * objectRequestBroker = new ObjectRequestBroker();
    myRouteStrategy = (RouteStrategy*) objectRequestBroker->getComponent(myRouteStrategyRegData->getID());
    myRouteStrategy->calculate();

// call calculate

}

