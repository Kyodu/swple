/**
 * @(#) Component.h
 */

#ifndef COMPONENT_H_H
#define COMPONENT_H_H

#include "ObjectRequestBroker.h"

class Component
{

public:
	void subscribe( long);

};

#endif

