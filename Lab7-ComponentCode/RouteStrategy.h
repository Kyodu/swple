/**
 * @(#) RouteStrategy.h
 */

#ifndef ROUTESTRATEGY_H_H
#define ROUTESTRATEGY_H_H

#include "Component.h"
#include "RouteStrategyRegData.h"
#include "iostream"
class RouteStrategy: public Component
{

public:
	virtual void calculate( ) = 0;


};

#endif
