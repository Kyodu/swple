/**
 * @(#) TruckRouteStrategy.h
 */

#ifndef TRUCKROUTESTRATEGY_H_H
#define TRUCKROUTESTRATEGY_H_H

#include "RouteStrategy.h"
class TruckRouteStrategy: public RouteStrategy
{
	
public:
	virtual void calculate( );
	
};

#endif
