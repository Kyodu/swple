/**
 * @(#) Component.cpp
 */


#include "Component.h"

void Component::subscribe(long ID )
{
    ObjectRequestBroker::subscribe(ID, this);
}

