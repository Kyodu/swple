#include <iostream>
using namespace std;

#include "DriverInfoSystem.h"
#include "TruckRouteStrategy.h"
#include "PassengerRouteStrategy.h"
#include "RouteStrategyRegData.h"
#include "BusRouteStrategy.h"

int main()
{
    cout << "Hello world!" << endl;

    //create desired strategy

    //TruckRouteStrategy *strategy = new TruckRouteStrategy();
    //PassengerRouteStrategy *strategy = new PassengerRouteStrategy();
    BusRouteStrategy *strategy = new BusRouteStrategy();
    // get the ID for the RouteStrategy
    int id = RouteStrategyRegData::getID();

    // register the strategy at the ORB
    strategy->subscribe(id);

    // Create an instance of the DriverInfoSystem
    DriverInfoSystem *driverInfoSystem = new DriverInfoSystem();

    return 0;
}
