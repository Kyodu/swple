//
// Created by Jan Niklas Hollenbeck on 07.12.18.
//

#ifndef SWPL_NAVIGATION_BUSROUTESTRATEGY_H
#define SWPL_NAVIGATION_BUSROUTESTRATEGY_H

#include "RouteStrategy.h"
class BusRouteStrategy: public RouteStrategy {


public:
    virtual void calculate( );

};


#endif //SWPL_NAVIGATION_BUSROUTESTRATEGY_H
